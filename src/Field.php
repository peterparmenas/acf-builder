<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class Field
 * @package PeterParmenas\AcfBuilder
 */
class Field
{
    /**
     * @var string
     */
    protected $key = "";

    /**
     * @var string
     */
    protected $label = "";

    /**
     * @var string
     */
    protected $name = "";

    /**
     * @var string
     */
    protected $type = "";

    /**
     * @var string
     */
    protected $instructions = "";

    /**
     * @var int
     */
    protected $required = 0;

    /**
     * @var int|FieldConditionalLogic
     */
    protected $conditionalLogic = 0;

    /**
     * @var FieldWrapper
     */
    protected $wrapper = [];

    /**
     * Field constructor.
     * @param string $name
     * @param string $key
     */
    public function __construct($name, $key = "")
    {
        $this->setKey($this->generateKey($name, $key));
        $this->setName($name);
        $this->setLabel($this->generateLabel($name));
        $this->wrapper = new FieldWrapper();
    }

    /**
     * @param string $key
     * @return Field
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string $name
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $label
     * @return Field
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param string $type
     * @return Field
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $instructions
     * @return Field
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
        return $this;
    }

    /**
     * @param int $required
     * @return Field
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @param FieldConditionalLogic $conditionalLogic
     * @return Field
     */
    public function setConditionalLogic($conditionalLogic)
    {
        $this->conditionalLogic = $conditionalLogic;
        return $this;
    }

    /**
     * @param string $width
     * @param string $class
     * @param string $id
     * @return Field
     */
    public function setWrapper($width = "", $class = "", $id = "")
    {
        $this->wrapper
            ->setWidth($width)
            ->setClass($class)
            ->setId($id);
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "key" => $this->key,
            "label" => $this->label,
            "name" => $this->name,
            "type" => $this->type,
            "instructions" => $this->instructions,
            "required" => $this->required,
            "conditional_logic" => $this->getConditionalLogicArray(),
            "wrapper" => $this->wrapper->toArray(),
        ];
    }

    /**
     * @param string $name
     * @param string $key
     * @return string
     */
    private function generateKey($name, $key = "")
    {
        if (empty($key)) {
            $key = $name;
        }
        if (strpos($key, "field_") !== 0) {
            $key = "field_$key";
        }
        return $key;
    }

    /**
     * @param string $name
     * @return string
     */
    private function generateLabel($name)
    {
        return ucfirst(str_replace(["-", "_"], " ", $name));
    }

    /**
     * @return int|array
     */
    private function getConditionalLogicArray()
    {
        if ($this->conditionalLogic instanceof FieldConditionalLogic) {
            return $this->conditionalLogic->toArray();
        }
        return 0;
    }
}
