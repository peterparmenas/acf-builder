<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class FieldWrapper
 * @package PeterParmenas\AcfBuilder
 */
class FieldWrapper
{
    /**
     * @var string
     */
    protected $width = "";

    /**
     * @var string
     */
    protected $class = "";

    /**
     * @var string
     */
    protected $id = "";

    /**
     * FieldWrapper constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $width
     * @return FieldWrapper
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @param string $class
     * @return FieldWrapper
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @param string $id
     * @return FieldWrapper
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "width" => $this->width,
            "class" => $this->class,
            "id" => $this->id,
        ];
    }
}
