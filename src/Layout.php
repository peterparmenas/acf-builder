<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class Layout
 * @package PeterParmenas\AcfBuilder
 */
class Layout
{
    /**
     * @var string
     */
    protected $key = "";

    /**
     * @var string
     */
    protected $name = "";

    /**
     * @var string
     */
    protected $label = "";

    /**
     * @var string
     */
    protected $display = "block";

    /**
     * @var Field[]
     */
    protected $subFields = [];

    /**
     * @var string
     */
    protected $min = "";

    /**
     * @var string
     */
    protected $max = "";

    /**
     * Layout constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setKey("layout_$name");
        $this->setName($name);
    }

    /**
     * @param string $key
     * @return Layout
     */
    private function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string $name
     * @return Layout
     */
    private function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $label
     * @return Layout
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param string $display
     * @return Layout
     */
    public function setDisplay($display)
    {
        $this->display = $display;
        return $this;
    }

    /**
     * @param Field[] $subFields
     * @return Layout
     */
    public function setSubFields($subFields)
    {
        $this->subFields = $subFields;
        return $this;
    }

    /**
     * @param string $min
     * @return Layout
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @param string $max
     * @return Layout
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "key" => $this->key,
            "name" => $this->name,
            "label" => $this->label,
            "display" => $this->display,
            "sub_fields" => $this->buildSubFields(),
            "min" => $this->min,
            "max" => $this->max,
        ];
    }

    /**
     * @return array
     */
    private function buildSubFields()
    {
        return array_map(function($subField){
            return $subField->toArray();
        }, $this->subFields);
    }
}
