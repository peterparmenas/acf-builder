<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class FieldConditionalLogic
 * @package PeterParmenas\AcfBuilder
 */
class FieldConditionalLogic
{
    /**
     * @var array
     */
    protected $conditionals = [];

    /**
     * FieldConditionalLogic constructor.
     * @param string $field
     * @param string $operator Accepts '==', '!=', '==empty', '!=empty', '==pattern', '==contains', '<', '>'.
     * @param string $value
     */
    public function __construct($field, $operator, $value = "")
    {
        $this->or($field, $operator, $value);
    }

    /**
     * @param string $field
     * @param string $operator
     * @param string $value
     * @return FieldConditionalLogic
     */
    public function or($field, $operator, $value = "")
    {
        $this->conditionals[] = [
            $this->getConditional($field, $operator, $value),
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $operator
     * @param string $value
     * @return FieldConditionalLogic
     */
    public function and($field, $operator, $value = "")
    {
        $count = count($this->conditionals);
        $last = $count - 1;
        $this->conditionals[$last][] = $this->getConditional($field, $operator, $value);
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->conditionals;
    }

    /**
     * @param string $field
     * @param string $operator
     * @param string $value
     * @return array
     */
    private function getConditional($field, $operator, $value = "")
    {
        $conditional = [
            "field" => $field,
            "operator" => $operator,
        ];
        if (!empty($value)) {
            $conditional["value"] = $value;
        }
        return $conditional;
    }
}
