<?php

namespace PeterParmenas\AcfBuilder;

use PeterParmenas\AcfBuilder\Field;
use PeterParmenas\AcfBuilder\TextField;

/**
 * Class FieldGroup
 * @package PeterParmenas\AcfBuilder
 */
class FieldGroup
{
    /**
     * @var string
     */
    public $key = "";

    /**
     * @var string
     */
    public $title = "";

    /**
     * @var Field[]
     */
    public $fields = [];

    /**
     * @var FieldGroupLocation
     */
    public $location = [];

    /**
     * @var int
     */
    public $menuOrder = 0;

    /**
     * @var string
     */
    public $position = "normal";

    /**
     * @var string
     */
    public $style = "default";

    /**
     * @var string
     */
    public $labelPlacement = "top";

    /**
     * @var string
     */
    public $instructionPlacement = "label";

    /**
     * @var string
     */
    public $hideOnScreen = "";

    /**
     * @var bool
     */
    public $active = true;

    /**
     * @var string
     */
    public $description = "";

    /**
     * FieldGroup constructor.
     * @param string $title
     * @param string $key
     */
    public function __construct($title, $key = "")
    {
        $this->setKey($this->generateKey($title, $key));
        $this->setTitle($title);
    }

    /**
     * @param string $key
     * @return FieldGroup
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string $title
     * @return FieldGroup
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param Field[] $fields
     * @return FieldGroup
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @param FieldGroupLocation $location
     * @return FieldGroup
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @param int $menuOrder
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @param string $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @param string $labelPlacement
     */
    public function setLabelPlacement($labelPlacement)
    {
        $this->labelPlacement = $labelPlacement;
    }

    /**
     * @param string $instructionPlacement
     */
    public function setInstructionPlacement($instructionPlacement)
    {
        $this->instructionPlacement = $instructionPlacement;
    }

    /**
     * @param string $hideOnScreen
     */
    public function setHideOnScreen($hideOnScreen)
    {
        $this->hideOnScreen = $hideOnScreen;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "key" => $this->key,
            "title" => $this->title,
            "fields" => $this->buildFields(),
            "location" => $this->buildLocation(),
            "menu_order" => $this->menuOrder,
            "position" => $this->position,
            "style" => $this->style,
            "label_placement" => $this->labelPlacement,
            "instruction_placement" => $this->instructionPlacement,
            "hide_on_screen" => $this->hideOnScreen,
            "active" => $this->active,
            "description" => $this->description,
        ];
    }

    /**
     * Build.
     */
    public function build()
    {
        $field_group = $this->toArray();
        acf_add_local_field_group($field_group);
    }

    /**
     * @param string $title
     * @param string $key
     * @return string
     */
    private function generateKey($title, $key = "")
    {
        if (empty($key)) {
            $key = str_replace([" "], ["_"], strtolower($title));
        }
        if (strpos($key, "group_") !== 0) {
            $key = "group_$key";
        }
        return $key;
    }

    /**
     * @param string $name
     * @return string
     */
    private function generateTitle($name)
    {
        $title = str_replace(["-", "_"], [" ", " "], $name);
        return ucfirst($title);
    }

    /**
     * @return array
     */
    private function buildFields()
    {
        return array_map(function($field){
            return $field->toArray();
        }, $this->fields);
    }

    /**
     * @return array
     */
    private function buildLocation()
    {
        if ($this->location instanceof FieldGroupLocation) {
            return $this->location->toArray();
        }
        return [];
    }
}
