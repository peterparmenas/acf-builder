<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class FieldGroupLocation
 * @package PeterParmenas\AcfBuilder
 */
class FieldGroupLocation
{
    /**
     * @var array
     */
    public $locations = [];

    /**
     * FieldGroupLocation constructor.
     * @param string $param
     * @param string $operator
     * @param string $value
     */
    public function __construct($param, $operator, $value)
    {
        if (!empty($param) && !empty($operator) && !empty($value)) {
            $this->or($param, $operator, $value);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->locations;
    }

    /**
     * @param string $param
     * @param string $operator
     * @param string $value
     * @return FieldGroupLocation
     */
    public function or($param, $operator, $value)
    {
        $this->locations[] = [
            $this->getLocation($param, $operator, $value),
        ];
        return $this;
    }

    /**
     * @param string $param
     * @param string $operator
     * @param string $value
     * @return FieldGroupLocation
     */
    public function and($param, $operator, $value)
    {
        $count = count($this->locations);
        $last = $count - 1;
        $this->locations[$last][] = $this->getLocation($param, $operator, $value);
        return $this;
    }

    /**
     * @param string $param
     * @param string $operator
     * @param string $value
     * @return array
     */
    private function getLocation($param, $operator, $value)
    {
        return [
            "param" => $param,
            "operator" => $operator,
            "value" => $value,
        ];
    }
}
