<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TabField
 * @package PeterParmenas\AcfBuilder
 */
class TabField extends Field
{
    /**
     * @var string
     */
    protected $type = "tab";

    /**
     * @var string
     */
    protected $placement = "top";

    /**
     * @var int
     */
    protected $endpoint = 0;

    /**
     * TabField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $placement Accepts 'top' or 'left'.
     * @return TabField
     */
    public function setPlacement($placement)
    {
        $this->placement = $placement;
        return $this;
    }

    /**
     * @param int $endpoint
     * @return TabField
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "placement" => $this->placement,
            "endpoint" => $this->endpoint,
        ]);
    }
}
