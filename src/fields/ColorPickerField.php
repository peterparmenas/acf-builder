<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class ColorPickerField
 * @package PeterParmenas\AcfBuilder
 */
class ColorPickerField extends Field
{
    /**
     * @var string
     */
    protected $type = "color_picker";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var int
     */
    protected $enableOpacity = 0;

    /**
     * @var string
     */
    protected $returnFormat = "string";

    /**
     * ColorPickerField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return ColorPickerField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param int $enableOpacity
     * @return ColorPickerField
     */
    public function setEnableOpacity($enableOpacity)
    {
        $this->enableOpacity = $enableOpacity;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'string' or 'array'.
     * @return ColorPickerField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["string", "array"])) {
            $returnFormat = "string";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "enable_opacity" => $this->enableOpacity,
            "return_format" => $this->returnFormat,
        ]);
    }
}
