<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class WysiwygField
 * @package PeterParmenas\AcfBuilder
 */
class WysiwygField extends Field
{
    /**
     * @var string
     */
    protected $type = "wysiwyg";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $tabs = "all";

    /**
     * @var string
     */
    protected $toolbar = "full";

    /**
     * @var int
     */
    protected $mediaUpload = 1;

    /**
     * @var int
     */
    protected $delay = 0;

    /**
     * WysiwygField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return WysiwygField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $tabs Accepts 'all', 'visual', or 'text'.
     * @return WysiwygField
     */
    public function setTabs($tabs)
    {
        if (!in_array($tabs, ["all", "visual", "text"])) {
            $tabs = "all";
        }
        $this->tabs = $tabs;
        return $this;
    }

    /**
     * @param string $toolbar Accepts 'full' or 'basic'.
     * @return WysiwygField
     */
    public function setToolbar($toolbar)
    {
        if (!in_array($toolbar, ["full", "basic"])) {
            $toolbar = "full";
        }
        $this->toolbar = $toolbar;
        return $this;
    }

    /**
     * @param int $mediaUpload
     * @return WysiwygField
     */
    public function setMediaUpload($mediaUpload)
    {
        $this->mediaUpload = $mediaUpload;
        return $this;
    }

    /**
     * @param int $delay
     * @return WysiwygField
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "tabs" => $this->tabs,
            "toolbar" => $this->toolbar,
            "media_upload" => $this->mediaUpload,
            "delay" => $this->delay,
        ]);
    }
}
