<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class PasswordField
 * @package PeterParmenas\AcfBuilder
 */
class PasswordField extends Field
{
    /**
     * @var string
     */
    protected $type = "password";

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * @var string
     */
    protected $prepend = "";

    /**
     * @var string
     */
    protected $append = "";

    /**
     * PasswordField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $placeholder
     * @return PasswordField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string $prepend
     * @return PasswordField
     */
    public function setPrepend($prepend)
    {
        $this->prepend = $prepend;
        return $this;
    }

    /**
     * @param string $append
     * @return PasswordField
     */
    public function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "placeholder" => $this->placeholder,
            "prepend" => $this->prepend,
            "append" => $this->append,
        ]);
    }
}
