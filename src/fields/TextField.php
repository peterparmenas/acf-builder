<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TextField
 * @package PeterParmenas\AcfBuilder
 */
class TextField extends Field
{
    /**
     * @var string
     */
    protected $type = "text";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * @var string
     */
    protected $prepend = "";

    /**
     * @var string
     */
    protected $append = "";

    /**
     * @var string|int
     */
    protected $maxLength = "";

    /**
     * TextField constructor.
     * @param string $name
     * @param string $key
     */
    public function __construct($name, $key = "")
    {
        parent::__construct($name, $key);
    }

    /**
     * @param string $defaultValue
     * @return TextField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return TextField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string $prepend
     * @return TextField
     */
    public function setPrepend($prepend)
    {
        $this->prepend = $prepend;
        return $this;
    }

    /**
     * @param string $append
     * @return TextField
     */
    public function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    /**
     * @param string|int $maxLength String if empty.
     * @return TextField
     */
    public function setMaxLength($maxLength)
    {
        if (empty($maxLength)) {
            $maxLength = "";
        }
        $this->maxLength = $maxLength;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "placeholder" => $this->placeholder,
            "prepend" => $this->prepend,
            "append" => $this->append,
            "maxlength" => $this->maxLength,
        ]);
    }
}
