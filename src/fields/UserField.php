<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class UserField
 * @package PeterParmenas\AcfBuilder
 */
class UserField extends Field
{
    /**
     * @var string
     */
    protected $type = "user";

    /**
     * @var string|string[]
     */
    protected $role = "";

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * @var int
     */
    protected $multiple = 0;

    /**
     * @var string
     */
    protected $returnFormat = "array";

    /**
     * UserField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|string[] $role String if empty.
     * @return UserField
     */
    public function setRole($role)
    {
        if (is_array($role) && empty($role)) {
            $role = "";
        }
        $this->role = $role;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return UserField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @param int $multiple
     * @return UserField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'array', 'object', or 'id'.
     * @return UserField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["array", "object", "id"])) {
            $returnFormat = "array";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "role" => $this->role,
            "allow_null" => $this->allowNull,
            "multiple" => $this->multiple,
            "return_format" => $this->returnFormat,
        ]);
    }
}
