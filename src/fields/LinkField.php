<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class LinkField
 * @package PeterParmenas\AcfBuilder
 */
class LinkField extends Field
{
    /**
     * @var string
     */
    protected $type = "link";

    /**
     * @var string
     */
    protected $returnFormat = "array";

    /**
     * LinkField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $returnFormat Accepts 'array' or 'url'.
     */
    public function setReturnFormat($returnFormat)
    {
        $this->returnFormat = $returnFormat;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "return_format" => $this->returnFormat,
        ]);
    }
}
