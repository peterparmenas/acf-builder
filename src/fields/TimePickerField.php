<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TimePickerField
 * @package PeterParmenas\AcfBuilder
 */
class TimePickerField extends Field
{
    /**
     * @var string
     */
    protected $type = "time_picker";

    /**
     * @var string
     */
    protected $displayFormat = "g:i:a";

    /**
     * @var string
     */
    protected $returnFormat = "g:i:a";

    /**
     * TimePickerField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $displayFormat
     * @return TimePickerField
     */
    public function setDisplayFormat($displayFormat)
    {
        $this->displayFormat = $displayFormat;
        return $this;
    }

    /**
     * @param string $returnFormat
     * @return TimePickerField
     */
    public function setReturnFormat($returnFormat)
    {
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "display_format" => $this->displayFormat,
            "return_format" => $this->returnFormat,
        ]);
    }
}
