<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class RepeaterField
 * @package PeterParmenas\AcfBuilder
 */
class RepeaterField extends Field
{
    /**
     * @var string
     */
    protected $type = "repeater";

    /**
     * @var string
     */
    protected $collapsed = "";

    /**
     * @var int
     */
    protected $min = 0;

    /**
     * @var int
     */
    protected $max = 0;

    /**
     * @var string
     */
    protected $layout = "table";

    /**
     * @var string
     */
    protected $buttonLabel = "";

    /**
     * @var Field[]
     */
    protected $subFields = [];

    /**
     * @param string $collapsed
     * @return RepeaterField
     */
    public function setCollapsed($collapsed)
    {
        $this->collapsed = $collapsed;
        return $this;
    }

    /**
     * @param int $min
     * @return RepeaterField
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @param int $max
     * @return RepeaterField
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @param string $buttonLabel
     * @return RepeaterField
     */
    public function setButtonLabel($buttonLabel)
    {
        $this->buttonLabel = $buttonLabel;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "collapsed" => $this->collapsed,
            "min" => $this->min,
            "max" => $this->max,
            "layout" => $this->layout,
            "button_label" => $this->buttonLabel,
            "sub_fields" => $this->buildSubFields(),
        ]);
    }

    /**
     * @return array
     */
    private function buildSubFields()
    {
        return array_map(function($subField){
            return $subField->toArray();
        }, $this->subFields);
    }
}
