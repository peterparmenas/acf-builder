<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class oEmbedField
 * @package PeterParmenas\AcfBuilder
 */
class oEmbedField extends Field
{
    /**
     * @var string
     */
    protected $type = "oembed";

    /**
     * @var string|int
     */
    protected $width = "";

    /**
     * @var string|int
     */
    protected $height = "";

    /**
     * oEmbedField constructor.
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|int $width String if empty.
     * @return oEmbedField
     */
    public function setWidth($width)
    {
        if (empty($width)) {
            $width = "";
        }
        $this->width = $width;
        return $this;
    }

    /**
     * @param string|int $height String if empty.
     * @return oEmbedField
     */
    public function setHeight($height)
    {
        if (empty($height)) {
            $height = "";
        }
        $this->height = $height;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "width" => $this->width,
            "height" => $this->height,
        ]);
    }
}
