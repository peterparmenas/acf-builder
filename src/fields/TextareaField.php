<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TextareaField
 * @package PeterParmenas\AcfBuilder
 */
class TextareaField extends Field
{
    /**
     * @var string
     */
    protected $type = "textarea";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * @var string|int
     */
    protected $maxLength = "";

    /**
     * @var string|int
     */
    protected $rows = "";

    /**
     * @var string
     */
    protected $newLines = "";

    /**
     * TextareaField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return TextareaField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return TextareaField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string|int $maxLength String if empty.
     * @return TextareaField
     */
    public function setMaxLength($maxLength)
    {
        if (empty($maxLength)) {
            $maxLength = "";
        }
        $this->maxLength = $maxLength;
        return $this;
    }

    /**
     * @param string|int $rows String if empty.
     * @return TextareaField
     */
    public function setRows($rows)
    {
        if (empty($rows)) {
            $rows = "";
        }
        $this->rows = $rows;
        return $this;
    }

    /**
     * @param string $newLines Accepts 'wpautop', 'br', or empty string.
     * @return TextareaField
     */
    public function setNewLines($newLines)
    {
        if (!empty($newLines) && !in_array($newLines, ["wpautop", "br"])) {
            $newLines = "";
        }
        $this->newLines = $newLines;
        return $this;
    }

    /**
     * @return array
     */
    public function to_array()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "placeholder" => $this->placeholder,
            "maxlength" => $this->maxLength,
            "rows" => $this->rows,
            "new_lines" => $this->newLines,
        ]);
    }
}
