<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class UrlField
 * @package PeterParmenas\AcfBuilder
 */
class UrlField extends Field
{
    /**
     * @var string
     */
    protected $type = "url";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * UrlField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return UrlField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return UrlField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "placeholder" => $this->placeholder,
        ]);
    }
}
