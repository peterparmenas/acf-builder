<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TrueFalseField
 * @package PeterParmenas\AcfBuilder
 */
class TrueFalseField extends Field
{
    /**
     * @var string
     */
    protected $type = "true_false";

    /**
     * @var string
     */
    protected $message = "";

    /**
     * @var int
     */
    protected $defaultValue = 0;

    /**
     * @var int
     */
    protected $ui = 0;

    /**
     * @var string
     */
    protected $uiOnText = "";

    /**
     * @var string
     */
    protected $uiOffText = "";

    /**
     * TrueFalseField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $message
     * @return TrueFalseField
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param int $defaultValue
     * @return TrueFalseField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param int $ui
     * @return TrueFalseField
     */
    public function setUi($ui)
    {
        $this->ui = $ui;
        return $this;
    }

    /**
     * @param string $uiOnText
     * @return TrueFalseField
     */
    public function setUiOnText($uiOnText)
    {
        $this->uiOnText = $uiOnText;
        return $this;
    }

    /**
     * @param string $uiOffText
     * @return TrueFalseField
     */
    public function setUiOffText($uiOffText)
    {
        $this->uiOffText = $uiOffText;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "message" => $this->message,
            "default_value" => $this->defaultValue,
            "ui" => $this->ui,
            "ui_on_text" => $this->uiOnText,
            "ui_off_text" => $this->uiOffText,
        ]);
    }
}
