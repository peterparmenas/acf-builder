<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class GoogleMapField
 * @package PeterParmenas\AcfBuilder
 */
class GoogleMapField extends Field
{
    /**
     * @var string
     */
    protected $type = "google_map";

    /**
     * @var string
     */
    protected $centerLat = "";

    /**
     * @var string
     */
    protected $centerLng = "";

    /**
     * @var string|int
     */
    protected $zoom = "";

    /**
     * @var string|int
     */
    protected $height = "";

    /**
     * GoogleMapField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $centerLat
     * @return GoogleMapField
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;
        return $this;
    }

    /**
     * @param string $centerLng
     * @return GoogleMapField
     */
    public function setCenterLng($centerLng)
    {
        $this->centerLng = $centerLng;
        return $this;
    }

    /**
     * @param string|int $zoom String if empty.
     * @return GoogleMapField
     */
    public function setZoom($zoom)
    {
        if (empty($zoom)) {
            $zoom = "";
        }
        $this->zoom = $zoom;
        return $this;
    }

    /**
     * @param string $height String if empty.
     * @return GoogleMapField
     */
    public function setHeight($height)
    {
        if (empty($height)) {
            $height = "";
        }
        $this->height = $height;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "center_lat" => $this->centerLat,
            "center_lng" => $this->centerLng,
            "zoom" => $this->zoom,
            "height" => $this->height,
        ]);
    }
}
