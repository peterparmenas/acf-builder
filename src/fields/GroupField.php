<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class GroupField
 * @package PeterParmenas\AcfBuilder
 */
class GroupField extends Field
{
    /**
     * @var string
     */
    protected $type = "group";

    /**
     * @var string
     */
    protected $layout = "block";

    /**
     * @var Field[]
     */
    protected $subFields = [];

    /**
     * GroupField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $layout Accepts 'block', 'table', or 'row'.
     * @return GroupField
     */
    public function setLayout($layout)
    {
        if (!in_array($layout, ["block", "table", "row"])) {
            $this->layout = $layout;
        }
        $this->layout = $layout;
        return $this;
    }

    /**
     * @param Field[] $subFields
     * @return GroupField
     */
    public function setSubFields($subFields)
    {
        $this->subFields = $subFields;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "layout" => $this->layout,
            "sub_fields" => $this->buildSubFields(),
        ]);
    }

    /**
     * @return array
     */
    private function buildSubFields()
    {
        return array_map(function($subField){
            return $subField->toArray();
        }, $this->subFields);
    }
}
