<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class RelationshipField
 * @package PeterParmenas\AcfBuilder
 */
class RelationshipField extends Field
{
    /**
     * @var string
     */
    protected $type = "relationship";

    /**
     * @var string|string[]
     */
    protected $postType = "";

    /**
     * @var string|string[]
     */
    protected $taxonomy = "";

    /**
     * @var string|string[]
     */
    protected $filters = ["search", "post_type", "taxonomy"];

    /**
     * @var string|string[]
     */
    protected $elements = "";

    /**
     * @var string|int
     */
    protected $min = "";

    /**
     * @var string|int
     */
    protected $max = "";

    /**
     * @var string
     */
    protected $returnFormat = "object";

    /**
     * PostObjectField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|string[] $postType String if empty.
     * @return RelationshipField
     */
    public function setPostType($postType)
    {
        if (is_array($postType) && empty($postType)) {
            $postType = "";
        }
        $this->postType = $postType;
        return $this;
    }

    /**
     * @param string|string[] $taxonomy Expects values in 'taxonomy_slug:term_slug' format. String if empty.
     * @return RelationshipField
     */
    public function setTaxonomy($taxonomy)
    {
        if (is_array($taxonomy) && empty($taxonomy)) {
            $taxonomy = "";
        }
        $this->taxonomy = $taxonomy;
        return $this;
    }

    /**
     * @param string|string[] $filters String if empty.
     * @return RelationshipField
     */
    public function setFilters($filters)
    {
        if (is_array($filters) && empty($filters)) {
            $filters = "";
        }
        $this->filters = $filters;
        return $this;
    }

    /**
     * @param string|string[] $elements String if empty.
     * @return RelationshipField
     */
    public function setElements($elements)
    {
        if (is_array($elements) && empty($elements)) {
            $elements = "";
        }
        $this->elements = $elements;
        return $this;
    }

    /**
     * @param string|int $min String if empty.
     * @return RelationshipField
     */
    public function setMin($min)
    {
        if (empty($min)) {
            $min = "";
        }
        $this->min = $min;
        return $this;
    }

    /**
     * @param string|int $max
     * @return RelationshipField
     */
    public function setMax($max)
    {
        if (empty($max)) {
            $max = "";
        }
        $this->max = $max;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'object' or 'id'.
     * @return RelationshipField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["object", "id"])) {
            $returnFormat = "object";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "post_type" => $this->postType,
            "taxonomy" => $this->taxonomy,
            "filters" => $this->filters,
            "elements" => $this->elements,
            "min" => $this->min,
            "max" => $this->max,
            "return_format" => $this->returnFormat,
        ]);
    }
}
