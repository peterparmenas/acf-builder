<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class ImageField
 * @package PeterParmenas\AcfBuilder
 */
class ImageField extends Field
{
    /**
     * @var string
     */
    protected $type = "image";

    /**
     * @var string
     */
    protected $returnFormat = "array";

    /**
     * @var string
     */
    protected $previewSize = "medium";

    /**
     * @var string
     */
    protected $library = "all";

    /**
     * @var string|int
     */
    protected $minWidth = "";

    /**
     * @var string|int
     */
    protected $minHeight = "";

    /**
     * @var string|int
     */
    protected $minSize = "";

    /**
     * @var string|int
     */
    protected $maxWidth = "";

    /**
     * @var string|int
     */
    protected $maxHeight = "";

    /**
     * @var string|int
     */
    protected $maxSize = "";

    /**
     * @var string
     */
    protected $mimeTypes = "";

    /**
     * ImageField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $returnFormat Accepts 'array', 'url', or 'id'.
     * @return ImageField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["array", "url", "id"])) {
            $returnFormat = "array";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param string $previewSize Accepts any valid image size name.
     * @return ImageField
     */
    public function setPreviewSize($previewSize)
    {
        $this->previewSize = $previewSize;
        return $this;
    }

    /**
     * @param string $library Accepts 'all' or 'uploadedTo'.
     * @return ImageField
     */
    public function setLibrary($library)
    {
        if (!in_array($library, ["all", "uploadedTo"])) {
            $library = "all";
        }
        $this->library = $library;
        return $this;
    }

    /**
     * @param string|int $minWidth String if empty.
     * @return ImageField
     */
    public function setMinWidth($minWidth)
    {
        if (empty($minWidth)) {
            $minWidth = "";
        }
        $this->minWidth = $minWidth;
        return $this;
    }

    /**
     * @param string|int $minHeight String if empty.
     * @return ImageField
     */
    public function setMinHeight($minHeight)
    {
        if (empty($minHeight)) {
            $minHeight = "";
        }
        $this->minHeight = $minHeight;
        return $this;
    }

    /**
     * @param string|int $minSize String if empty.
     * @return ImageField
     */
    public function setMinSize($minSize)
    {
        if (empty($minSize)) {
            $minSize = "";
        }
        $this->minSize = $minSize;
        return $this;
    }

    /**
     * @param string|int $maxWidth String if empty.
     * @return ImageField
     */
    public function setMaxWidth($maxWidth)
    {
        if (empty($maxWidth)) {
            $maxWidth = "";
        }
        $this->maxWidth = $maxWidth;
        return $this;
    }

    /**
     * @param string|int $maxHeight String if empty.
     * @return ImageField
     */
    public function setMaxHeight($maxHeight)
    {
        if (empty($maxHeight)) {
            $maxHeight = "";
        }
        $this->maxHeight = $maxHeight;
        return $this;
    }

    /**
     * @param string|int $maxSize String if empty.
     * @return ImageField
     */
    public function setMaxSize($maxSize)
    {
        if (empty($maxSize)) {
            $maxSize = "";
        }
        $this->maxSize = $maxSize;
        return $this;
    }

    /**
     * @param string $mimeTypes Comma separated string.
     * @return ImageField
     */
    public function setMimeTypes($mimeTypes)
    {
        $this->mimeTypes = $mimeTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "return_format" => $this->returnFormat,
            "preview_size" => $this->previewSize,
            "library" => $this->library,
            "min_width" => $this->minWidth,
            "min_height" => $this->minHeight,
            "min_size" => $this->minSize,
            "max_width" => $this->maxWidth,
            "max_height" => $this->maxHeight,
            "max_size" => $this->maxSize,
            "mime_types" => $this->mimeTypes,
        ]);
    }
}
