<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class FileField
 * @package PeterParmenas\AcfBuilder
 */
class FileField extends Field
{
    /**
     * @var string
     */
    protected $type = "file";

    /**
     * @var string
     */
    protected $returnFormat = "array";

    /**
     * @var string
     */
    protected $library = "all";

    /**
     * @var string|int
     */
    protected $minSize = "";

    /**
     * @var string|int
     */
    protected $maxSize = "";

    /**
     * @var string
     */
    protected $mimeTypes = "";

    /**
     * FileField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $returnFormat Accepts 'array', 'url', or 'id'.
     * @return FileField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["array", "url", "id"])) {
            $returnFormat = "array";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param string $library Accepts 'all' or 'uploadedTo'.
     * @return FileField
     */
    public function setLibrary($library)
    {
        if (!in_array($library, ["all", "uploadedTo"])) {
            $library = "all";
        }
        $this->library = $library;
        return $this;
    }

    /**
     * @param string|int $minSize String if empty.
     * @return FileField
     */
    public function setMinSize($minSize)
    {
        if (empty($minSize)) {
            $minSize = "";
        }
        $this->minSize = $minSize;
        return $this;
    }

    /**
     * @param string|int $maxSize String if empty.
     * @return FileField
     */
    public function setMaxSize($maxSize)
    {
        if (empty($maxSize)) {
            $maxSize = "";
        }
        $this->maxSize = $maxSize;
        return $this;
    }

    /**
     * @param string $mimeTypes Comma separated string.
     * @return FileField
     */
    public function setMimeTypes($mimeTypes)
    {
        $this->mimeTypes = $mimeTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "return_format" => $this->returnFormat,
            "library" => $this->library,
            "min_size" => $this->minSize,
            "max_size" => $this->maxSize,
            "mime_types" => $this->mimeTypes,
        ]);
    }
}
