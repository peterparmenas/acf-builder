<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class MessageField
 * @package PeterParmenas\AcfBuilder
 */
class MessageField extends Field
{
    /**
     * @var string
     */
    protected $type = "message";

    /**
     * @var string
     */
    protected $message = "";

    /**
     * @var string
     */
    protected $newLines = "wpautop";

    /**
     * @var int
     */
    protected $escHtml = 0;

    /**
     * MessageField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $message
     * @return MessageField
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param string $newLines Accepts 'wpautop', 'br', or empty string.
     * @return MessageField
     */
    public function setNewLines($newLines)
    {
        if (!empty($newLines) && !in_array($newLines, ["wpautop", "br"])) {
            $newLines = "";
        }
        $this->newLines = $newLines;
        return $this;
    }

    /**
     * @param int $escHtml
     * @return MessageField
     */
    public function setEscHtml($escHtml)
    {
        $this->escHtml = $escHtml;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "message" => $this->message,
            "new_lines" => $this->newLines,
            "esc_html" => $this->escHtml,
        ]);
    }
}
