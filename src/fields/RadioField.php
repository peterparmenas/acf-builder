<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class RadioField
 * @package PeterParmenas\AcfBuilder
 */
class RadioField extends Field
{
    /**
     * @var string
     */
    protected $type = "radio";

    /**
     * @var array
     */
    protected $choices = [];

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * @var int
     */
    protected $otherChoice = 0;

    /**
     * @var int
     */
    protected $saveOtherChoice = 0;

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $layout = "vertical";

    /**
     * @var string
     */
    protected $returnFormat = "value";

    /**
     * RadioField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param array $choices
     * @return RadioField
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return RadioField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @param int $otherChoice
     * @return RadioField
     */
    public function setOtherChoice($otherChoice)
    {
        $this->otherChoice = $otherChoice;
        return $this;
    }

    /**
     * @param int $saveOtherChoice
     * @return RadioField
     */
    public function setSaveOtherChoice($saveOtherChoice)
    {
        $this->saveOtherChoice = $saveOtherChoice;
        return $this;
    }

    /**
     * @param string $defaultValue
     * @return RadioField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $layout Accepts 'vertical' or 'horizontal'.
     * @return RadioField
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'value', 'label', or 'array'.
     * @return RadioField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["value", "label", "array"])) {
            $returnFormat = "value";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "choices" => $this->choices,
            "allow_null" => $this->allowNull,
            "other_choice" => $this->otherChoice,
            "save_other_choice" => $this->saveOtherChoice,
            "default_value" => $this->defaultValue,
            "layout" => $this->layout,
            "return_format" => $this->returnFormat,
        ]);
    }
}
