<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class RangeField
 * @package PeterParmenas\AcfBuilder
 */
class RangeField extends Field
{
    /**
     * @var string
     */
    protected $type = "range";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string|int
     */
    protected $min = "";

    /**
     * @var string|int
     */
    protected $max = "";

    /**
     * @var string|int
     */
    protected $step = "";

    /**
     * @var string
     */
    protected $prepend = "";

    /**
     * @var string
     */
    protected $append = "";

    /**
     * RangeField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return RangeField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return RangeField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string|int $min String if empty.
     * @return RangeField
     */
    public function setMin($min)
    {
        if (empty($min)) {
            $min = "";
        }
        $this->min = $min;
        return $this;
    }

    /**
     * @param string|int $max String if empty.
     * @return RangeField
     */
    public function setMax($max)
    {
        if (empty($max)) {
            $max = "";
        }
        $this->max = $max;
        return $this;
    }

    /**
     * @param string|int $step String if empty.
     * @return RangeField
     */
    public function setStep($step)
    {
        if (empty($step)) {
            $step = "";
        }
        $this->step = $step;
        return $this;
    }

    /**
     * @param string $prepend
     * @return RangeField
     */
    public function setPrepend($prepend)
    {
        $this->prepend = $prepend;
        return $this;
    }

    /**
     * @param string $append
     * @return RangeField
     */
    public function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "prepend" => $this->prepend,
            "append" => $this->append,
            "min" => $this->min,
            "max" => $this->max,
            "step" => $this->step,
        ]);
    }
}
