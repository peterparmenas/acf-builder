<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class DateTimePickerField
 * @package PeterParmenas\AcfBuilder
 */
class DateTimePickerField extends Field
{
    /**
     * @var string
     */
    protected $type = "date_time_picker";

    /**
     * @var string
     */
    protected $displayFormat = "d/m/Y g:i:a";

    /**
     * @var string
     */
    protected $returnFormat = "d/m/Y g:i:a";

    /**
     * @var int
     */
    protected $firstDay = 1;

    /**
     * ACF_Date_Picker_Field constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $displayFormat
     * @return DateTimePickerField
     */
    public function setDisplayFormat($displayFormat)
    {
        $this->displayFormat = $displayFormat;
        return $this;
    }

    /**
     * @param string $returnFormat
     * @return DateTimePickerField
     */
    public function setReturnFormat($returnFormat)
    {
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param int $firstDay
     * @return DateTimePickerField
     */
    public function setFirstDay($firstDay)
    {
        $this->firstDay = $firstDay;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "display_format" => $this->displayFormat,
            "return_format" => $this->returnFormat,
            "first_day" => $this->firstDay,
        ]);
    }
}
