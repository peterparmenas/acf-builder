<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class CheckboxField
 * @package PeterParmenas\AcfBuilder
 */
class CheckboxField extends Field
{
    /**
     * @var string
     */
    protected $type = "checkbox";

    /**
     * @var array
     */
    protected $choices = [];

    /**
     * @var int
     */
    protected $allowCustom = 0;

    /**
     * @var int
     */
    protected $saveCustom = 0;

    /**
     * @var string[]
     */
    protected $defaultValue = [];

    /**
     * @var string
     */
    protected $layout = "vertical";

    /**
     * @var int
     */
    protected $toggle = 0;

    /**
     * @var string
     */
    protected $returnFormat = "value";

    /**
     * CheckboxField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param array $choices
     * @return CheckboxField
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @param int $allowCustom
     * @return CheckboxField
     */
    public function setAllowCustom($allowCustom)
    {
        $this->allowCustom = $allowCustom;
        return $this;
    }

    /**
     * @param int $saveCustom
     * @return CheckboxField
     */
    public function setSaveCustom($saveCustom)
    {
        $this->saveCustom = $saveCustom;
        return $this;
    }

    /**
     * @param string[] $defaultValue
     * @return CheckboxField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $layout Accepts 'vertical' or 'horizontal'.
     * @return CheckboxField
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @param int $toggle
     * @return CheckboxField
     */
    public function setToggle($toggle)
    {
        $this->toggle = $toggle;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'value', 'label', or 'array'.
     * @return CheckboxField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["value", "label", "array"])) {
            $returnFormat = "value";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "choices" => $this->choices,
            "allow_custom" => $this->allowCustom,
            "save_custom" => $this->saveCustom,
            "default_value" => $this->defaultValue,
            "layout" => $this->layout,
            "toggle" => $this->toggle,
            "return_format" => $this->returnFormat,
        ]);
    }
}
