<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class AccordionField
 * @package PeterParmenas\AcfBuilder
 */
class AccordionField extends Field
{
    /**
     * @var string
     */
    protected $type = "accordion";

    /**
     * @var int
     */
    protected $open = 0;

    /**
     * @var int
     */
    protected $multiExpand = 0;

    /**
     * @var int
     */
    protected $endpoint = 0;

    /**
     * AccordionField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param int $open
     * @return AccordionField
     */
    public function setOpen($open)
    {
        $this->open = $open;
        return $this;
    }

    /**
     * @param int $multiExpand
     * @return AccordionField
     */
    public function setMultiExpand($multiExpand)
    {
        $this->multiExpand = $multiExpand;
        return $this;
    }

    /**
     * @param int $endpoint
     * @return AccordionField
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "open" => $this->open,
            "multi_expand" => $this->multiExpand,
            "endpoint" => $this->endpoint,
        ]);
    }
}
