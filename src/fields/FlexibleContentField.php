<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class FlexibleContentField
 * @package PeterParmenas\AcfBuilder
 */
class FlexibleContentField extends Field
{
    /**
     * @var string
     */
    protected $type = "flexible_content";

    /**
     * @var Layout[]
     */
    protected $layouts = [];

    /**
     * @var string
     */
    protected $buttonLabel = "";

    /**
     * @var string|int
     */
    protected $min = "";

    /**
     * @var string|int
     */
    protected $max = "";

    /**
     * FlexibleContentField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param Layout[] $layouts
     * @return FlexibleContentField
     */
    public function setLayouts($layouts)
    {
        $this->layouts = $layouts;
        return $this;
    }

    /**
     * @param string $buttonLabel
     * @return FlexibleContentField
     */
    public function setButtonLabel($buttonLabel)
    {
        $this->buttonLabel = $buttonLabel;
        return $this;
    }

    /**
     * @param string|int $min String if empty.
     * @return FlexibleContentField
     */
    public function setMin($min)
    {
        if (empty($min)) {
            $min = "";
        }
        $this->min = $min;
        return $this;
    }

    /**
     * @param string|int $max String if empty.
     * @return FlexibleContentField
     */
    public function setMax($max)
    {
        if (empty($max)) {
            $max = "";
        }
        $this->max = $max;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "layouts" => $this->buildLayouts(),
            "button_label" => $this->buttonLabel,
            "min" => $this->min,
            "max" => $this->max,
        ]);
    }

    /**
     * @return array
     */
    private function buildLayouts()
    {
        $layouts = [];
        foreach ($this->layouts as $layout) {
            $layoutArray = $layout->toArray();
            $layouts[$layoutArray["key"]] = $layoutArray;
        }
        return $layouts;
    }
}
