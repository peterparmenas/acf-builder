<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class TaxonomyField
 * @package PeterParmenas\AcfBuilder
 */
class TaxonomyField extends Field
{
    /**
     * @var string
     */
    protected $type = "taxonomy";

    /**
     * @var string
     */
    protected $taxonomy = "category";

    /**
     * @var string
     */
    protected $fieldType = "checkbox";

    /**
     * @var int
     */
    protected $addTerm = 1;

    /**
     * @var int
     */
    protected $saveTerms = 0;

    /**
     * @var int
     */
    protected $loadTerms = 0;

    /**
     * @var string
     */
    protected $returnFormat = "id";

    /**
     * @var int
     */
    protected $multiple = 0;

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * TaxonomyField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $taxonomy
     * @return TaxonomyField
     */
    public function setTaxonomy($taxonomy)
    {
        $this->taxonomy = $taxonomy;
        return $this;
    }

    /**
     * @param string $fieldType Accepts 'checkbox', 'multi_select', 'radio', or 'select'.
     * @return TaxonomyField
     */
    public function setFieldType($fieldType)
    {
        if (!in_array($fieldType, ["checkbox", "multi_select", "radio", "select"])) {
            $fieldType = "checkbox";
        }
        $this->fieldType = $fieldType;
        return $this;
    }

    /**
     * @param int $addTerm
     * @return TaxonomyField
     */
    public function setAddTerm($addTerm)
    {
        $this->addTerm = $addTerm;
        return $this;
    }

    /**
     * @param int $saveTerms
     * @return TaxonomyField
     */
    public function setSaveTerms($saveTerms)
    {
        $this->saveTerms = $saveTerms;
        return $this;
    }

    /**
     * @param int $loadTerms
     * @return TaxonomyField
     */
    public function setLoadTerms($loadTerms)
    {
        $this->loadTerms = $loadTerms;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'object' or 'id'.
     * @return TaxonomyField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["object", "id"])) {
            $returnFormat = "id";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param int $multiple
     * @return TaxonomyField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return TaxonomyField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "taxonomy" => $this->taxonomy,
            "field_type" => $this->fieldType,
            "add_term" => $this->addTerm,
            "save_terms" => $this->saveTerms,
            "load_terms" => $this->loadTerms,
            "return_format" => $this->returnFormat,
            "multiple" => $this->multiple,
            "allow_null" => $this->allowNull,
        ]);
    }
}
