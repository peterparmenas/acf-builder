<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class ButtonGroupField
 * @package PeterParmenas\AcfBuilder
 */
class ButtonGroupField extends Field
{
    /**
     * @var string
     */
    protected $type = "button_group";

    /**
     * @var array
     */
    protected $choices = [];

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $layout = "horizontal";

    /**
     * @var string
     */
    protected $returnFormat = "value";

    /**
     * ButtonGroupField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param array $choices
     * @return ButtonGroupField
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return ButtonGroupField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @param string $defaultValue
     * @return ButtonGroupField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $layout Accepts 'vertical' or 'horizontal'.
     * @return ButtonGroupField
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'value', 'label', or 'array'.
     * @return ButtonGroupField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["value", "label", "array"])) {
            $returnFormat = "value";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "choices" => $this->choices,
            "allow_null" => $this->allowNull,
            "default_value" => $this->defaultValue,
            "layout" => $this->layout,
            "return_format" => $this->returnFormat,
        ]);
    }
}
