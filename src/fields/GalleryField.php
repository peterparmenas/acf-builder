<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class GalleryField
 * @package PeterParmenas\AcfBuilder
 */
class GalleryField extends Field
{
    /**
     * @var string
     */
    protected $type = "gallery";

    /**
     * @var string
     */
    protected $returnFormat = "array";

    /**
     * @var string
     */
    protected $previewSize = "medium";

    /**
     * @var string
     */
    protected $insert = "append";

    /**
     * @var string
     */
    protected $library = "all";

    /**
     * @var string|int
     */
    protected $min = "";

    /**
     * @var string|int
     */
    protected $max = "";

    /**
     * @var string|int
     */
    protected $minWidth = "";

    /**
     * @var string|int
     */
    protected $minHeight = "";

    /**
     * @var string|int
     */
    protected $minSize = "";

    /**
     * @var string|int
     */
    protected $maxWidth = "";

    /**
     * @var string|int
     */
    protected $maxHeight = "";

    /**
     * @var string|int
     */
    protected $maxSize = "";

    /**
     * @var string
     */
    protected $mimeTypes = "";

    /**
     * GalleryField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $returnFormat Accepts 'array', 'url', or 'id'.
     * @return GalleryField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["array", "url", "id"])) {
            $returnFormat = "array";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param string $previewSize Accepts any valid image size name.
     * @return GalleryField
     */
    public function setPreviewSize($previewSize)
    {
        $this->previewSize = $previewSize;
        return $this;
    }

    /**
     * @param string $insert Accepts 'append' or 'prepend'.
     * @return GalleryField
     */
    public function setInsert($insert)
    {
        if (!in_array($insert, ["append", "prepend"])) {
            $insert = "append";
        }
        $this->insert = $insert;
        return $this;
    }

    /**
     * @param string $library Accepts 'all' or 'uploadedTo'.
     * @return GalleryField
     */
    public function setLibrary($library)
    {
        if (!in_array($library, ["all", "uploadedTo"])) {
            $library = "all";
        }
        $this->library = $library;
        return $this;
    }

    /**
     * @param string|int $min String if empty.
     * @return GalleryField
     */
    public function setMin($min)
    {
        if (empty($min)) {
            $min = "";
        }
        $this->min = $min;
        return $this;
    }

    /**
     * @param string|int $max String if empty.
     * @return GalleryField
     */
    public function setMax($max)
    {
        if (empty($max)) {
            $max = "";
        }
        $this->max = $max;
        return $this;
    }

    /**
     * @param string|int $minWidth String if empty.
     * @return GalleryField
     */
    public function setMinWidth($minWidth)
    {
        if (empty($minWidth)) {
            $minWidth = "";
        }
        $this->minWidth = $minWidth;
        return $this;
    }

    /**
     * @param string|int $minHeight String if empty.
     * @return GalleryField
     */
    public function setMinHeight($minHeight)
    {
        if (empty($minHeight)) {
            $minHeight = "";
        }
        $this->minHeight = $minHeight;
        return $this;
    }

    /**
     * @param string|int $minSize String if empty.
     * @return GalleryField
     */
    public function setMinSize($minSize)
    {
        if (empty($minSize)) {
            $minSize = "";
        }
        $this->minSize = $minSize;
        return $this;
    }

    /**
     * @param string|int $maxWidth String if empty.
     * @return GalleryField
     */
    public function setMaxWidth($maxWidth)
    {
        if ($maxWidth) {
            $maxWidth = "";
        }
        $this->maxWidth = $maxWidth;
        return $this;
    }

    /**
     * @param string|int $maxHeight String if empty.
     * @return GalleryField
     */
    public function setMaxHeight($maxHeight)
    {
        if (empty($maxHeight)) {
            $maxHeight = "";
        }
        $this->maxHeight = $maxHeight;
        return $this;
    }

    /**
     * @param string|int $maxSize String if empty.
     * @return GalleryField
     */
    public function setMaxSize($maxSize)
    {
        if (empty($maxSize)) {
            $maxSize = "";
        }
        $this->maxSize = $maxSize;
        return $this;
    }

    /**
     * @param string $mimeTypes Comma separated string.
     * @return GalleryField
     */
    public function setMimeTypes($mimeTypes)
    {
        $this->mimeTypes = $mimeTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "return_format" => $this->returnFormat,
            "preview_size" => $this->previewSize,
            "insert" => $this->insert,
            "library" => $this->library,
            "min" => $this->min,
            "max" => $this->max,
            "min_width" => $this->minWidth,
            "min_height" => $this->minHeight,
            "min_size" => $this->minSize,
            "max_width" => $this->maxWidth,
            "max_height" => $this->maxHeight,
            "max_size" => $this->maxSize,
            "mime_types" => $this->mimeTypes,
        ]);
    }
}
