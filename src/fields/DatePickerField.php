<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class DatePickerField
 * @package PeterParmenas\AcfBuilder
 */
class DatePickerField extends Field
{
    /**
     * @var string
     */
    protected $type = "date_picker";

    /**
     * @var string
     */
    protected $displayFormat = "d/m/Y";

    /**
     * @var string
     */
    protected $returnFormat = "d/m/Y";

    /**
     * @var int
     */
    protected $firstDay = 1;

    /**
     * ACF_Date_Picker_Field constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $displayFormat
     * @return DatePickerField
     */
    public function setDisplayFormat($displayFormat)
    {
        $this->displayFormat = $displayFormat;
        return $this;
    }

    /**
     * @param string $returnFormat
     * @return DatePickerField
     */
    public function setReturnFormat($returnFormat)
    {
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param int $firstDay
     * @return DatePickerField
     */
    public function setFirstDay($firstDay)
    {
        $this->firstDay = $firstDay;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "display_format" => $this->displayFormat,
            "return_format" => $this->returnFormat,
            "first_day" => $this->firstDay,
        ]);
    }
}
