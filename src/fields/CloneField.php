<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class CloneField
 * @package PeterParmenas\AcfBuilder
 */
class CloneField extends Field
{
    /**
     * @var string
     */
    protected $type = "clone";

    /**
     * @var string|string[]
     */
    protected $clone = "";

    /**
     * @var string
     */
    protected $display = "seamless";

    /**
     * @var string
     */
    protected $layout = "block";

    /**
     * @var int
     */
    protected $prefixLabel = 0;

    /**
     * @var int
     */
    protected $prefixName = 0;

    /**
     * CloneField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|string[] $clone
     * @return CloneField
     */
    public function setClone($clone)
    {
        $this->clone = $clone;
        return $this;
    }

    /**
     * @param string $display Accepts 'group' or 'seamless'.
     * @return CloneField
     */
    public function setDisplay($display)
    {
        if (!in_array($display, ["group", "seamless"])) {
            $display = "group";
        }
        $this->display = $display;
        return $this;
    }

    /**
     * @param string $layout
     * @return CloneField
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @param int $prefixLabel
     * @return CloneField
     */
    public function setPrefixLabel($prefixLabel)
    {
        $this->prefixLabel = $prefixLabel;
        return $this;
    }

    /**
     * @param int $prefixName
     * @return CloneField
     */
    public function setPrefixName($prefixName)
    {
        $this->prefixName = $prefixName;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "clone" => $this->clone,
            "display" => $this->display,
            "layout" => $this->layout,
            "prefix_label" => $this->prefixLabel,
            "prefix_name" => $this->prefixName,
        ]);
    }
}
