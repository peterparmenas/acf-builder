<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class PageLinkField
 * @package PeterParmenas\AcfBuilder
 */
class PageLinkField extends Field
{
    /**
     * @var string
     */
    protected $type = "page_link";

    /**
     * @var string|string[]
     */
    protected $postType = "";

    /**
     * @var string|string[]
     */
    protected $taxonomy = "";

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * @var int
     */
    protected $allowArchives = 1;

    /**
     * @var int
     */
    protected $multiple = 0;

    /**
     * ACF_Page_Link_Field constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|string[] $postType String if empty.
     * @return PageLinkField
     */
    public function setPostType($postType)
    {
        if (is_array($postType) && empty($postType)) {
            $postType = "";
        }
        $this->postType = $postType;
        return $this;
    }

    /**
     * @param string|string[] $taxonomy Expects values in 'taxonomy_slug:term_slug' format. String if empty.
     * @return PageLinkField
     */
    public function setTaxonomy($taxonomy)
    {
        if (is_array($taxonomy) && empty($taxonomy)) {
            $taxonomy = "";
        }
        $this->taxonomy = $taxonomy;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return PageLinkField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @param int $allowArchives
     * @return PageLinkField
     */
    public function setAllowArchives($allowArchives)
    {
        $this->allowArchives = $allowArchives;
        return $this;
    }

    /**
     * @param int $multiple
     * @return PageLinkField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "post_type" => $this->postType,
            "taxonomy" => $this->taxonomy,
            "allow_null" => $this->allowNull,
            "allow_archives" => $this->allowArchives,
            "multiple" => $this->multiple,
        ]);
    }
}
