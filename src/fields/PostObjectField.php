<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class PostObjectField
 * @package PeterParmenas\AcfBuilder
 */
class PostObjectField extends Field
{
    /**
     * @var string
     */
    protected $type = "post_object";

    /**
     * @var string|string[]
     */
    protected $postType = "";

    /**
     * @var string|string[]
     */
    protected $taxonomy = "";

    /**
     * @var int
     */
    protected $allow_null = 0;

    /**
     * @var int
     */
    protected $multiple = 0;

    /**
     * @var string
     */
    protected $return_format = "object";

    /**
     * @var int
     */
    protected $ui = 1;

    /**
     * PostObjectField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string|string[] $postType String if empty.
     * @return PostObjectField
     */
    public function setPostType($postType)
    {
        if (is_array($postType) && empty($postType)) {
            $postType = "";
        }
        $this->postType = $postType;
        return $this;
    }

    /**
     * @param string|string[] $taxonomy Expects values in 'taxonomy_slug:term_slug' format. String if empty.
     * @return PostObjectField
     */
    public function setTaxonomy($taxonomy)
    {
        if (is_array($taxonomy) && empty($taxonomy)) {
            $taxonomy = "";
        }
        $this->taxonomy = $taxonomy;
        return $this;
    }

    /**
     * @param int $allow_null
     * @return PostObjectField
     */
    public function setAllowNull($allow_null)
    {
        $this->allow_null = $allow_null;
        return $this;
    }

    /**
     * @param int $multiple
     * @return PostObjectField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param string $return_format Accepts 'object' or 'id'.
     * @return PostObjectField
     */
    public function setReturnFormat($return_format)
    {
        if (!in_array($return_format, ["object", "id"])) {
            $return_format = "object";
        }
        $this->return_format = $return_format;
        return $this;
    }

    /**
     * @param int $ui
     * @return PostObjectField
     */
    public function setUi($ui)
    {
        $this->ui = $ui;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "post_type" => $this->postType,
            "taxonomy" => $this->taxonomy,
            "allow_null" => $this->allow_null,
            "multiple" => $this->multiple,
            "return_format" => $this->return_format,
            "ui" => $this->ui,
        ]);
    }
}
