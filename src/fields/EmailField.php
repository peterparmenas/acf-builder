<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class EmailField
 * @package PeterParmenas\AcfBuilder
 */
class EmailField extends Field
{
    /**
     * @var string
     */
    protected $type = "email";

    /**
     * @var string
     */
    protected $defaultValue = "";

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * @var string
     */
    protected $prepend = "";

    /**
     * @var string
     */
    protected $append = "";

    /**
     * EmailField constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * @param string $defaultValue
     * @return EmailField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return EmailField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string $prepend
     * @return EmailField
     */
    public function setPrepend($prepend)
    {
        $this->prepend = $prepend;
        return $this;
    }

    /**
     * @param string $append
     * @return EmailField
     */
    public function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "default_value" => $this->defaultValue,
            "placeholder" => $this->placeholder,
            "prepend" => $this->prepend,
            "append" => $this->append,
        ]);
    }
}
