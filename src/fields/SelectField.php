<?php

namespace PeterParmenas\AcfBuilder;

/**
 * Class SelectField
 * @package PeterParmenas\AcfBuilder
 */
class SelectField extends Field
{
    /**
     * @var string
     */
    protected $type = "select";

    /**
     * @var array
     */
    protected $choices = [];

    /**
     * @var string|int|bool|string[]
     */
    protected $defaultValue = false;

    /**
     * @var int
     */
    protected $allowNull = 0;

    /**
     * @var int
     */
    protected $multiple = 0;

    /**
     * @var int
     */
    protected $ui = 0;

    /**
     * @var string
     */
    protected $returnFormat = "value";

    /**
     * @var int
     */
    protected $ajax = 0;

    /**
     * @var string
     */
    protected $placeholder = "";

    /**
     * SelectField constructor.
     * @param string $name
     * @param string $key
     */
    public function __construct($name, $key = "")
    {
        parent::__construct($name, $key);
    }

    /**
     * @param array $choices
     * @return SelectField
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @param string|int|bool|string[] $defaultValue Array if multiple is true.
     * @return SelectField
     */
    public function setDefaultValue($defaultValue)
    {
        if (!empty($defaultValue)) {
            if ($this->multiple && !is_array($defaultValue)) {
                $defaultValue = [$defaultValue];
            }
        }
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @param int $allowNull
     * @return SelectField
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
        return $this;
    }

    /**
     * @param int $multiple
     * @return SelectField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param int $ui
     * @return SelectField
     */
    public function setUi($ui)
    {
        $this->ui = $ui;
        return $this;
    }

    /**
     * @param string $returnFormat Accepts 'value', 'label', or 'array'.
     * @return SelectField
     */
    public function setReturnFormat($returnFormat)
    {
        if (!in_array($returnFormat, ["value", "label", "array"])) {
            $returnFormat = "value";
        }
        $this->returnFormat = $returnFormat;
        return $this;
    }

    /**
     * @param int $ajax
     * @return SelectField
     */
    public function setAjax($ajax)
    {
        $this->ajax = $ajax;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return SelectField
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            "choices" => $this->choices,
            "default_value" => $this->defaultValue,
            "allow_null" => $this->allowNull,
            "multiple" => $this->multiple,
            "ui" => $this->ui,
            "return_format" => $this->returnFormat,
            "ajax" => $this->ajax,
            "placeholder" => $this->placeholder,
        ]);
    }
}
